package ru.aliya12.uitestsproject.base;

/**
 * Created on 13.12.2022
 *
 * @author Aliya Kuznetsova (aliyakuzn@gmail.com)
 */
public final class Key {
    public static final String F_SYSTEM_NUMBER = "F_SYSTEM_NUMBER";
    public static final String MAINTENANCE_STRATEGY_ID = "MAINTENANCE_STRATEGY_ID";
}
