package ru.aliya12.uitestsproject.base;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Allure;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.lang.reflect.Constructor;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.util.Objects.requireNonNull;

/**
 * Created on 08.12.2022
 *
 * @author Aliya Kuznetsova (aliyakuzn@gmail.com)
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class BasePage<T extends BasePage> {
    protected static final Map<String, Object> GLOBAL_CONTEXT = new ConcurrentHashMap<>();

    @NotNull
    protected final Map<String, Object> context;

    public BasePage() {
        this.context = new HashMap<>();
    }

    public BasePage(@NotNull Map<String, Object> context) {
        this.context = requireNonNull(context);
    }

    protected void put(@NotNull String key, @NotNull Object value) {
        context.put(requireNonNull(key), requireNonNull(value));
    }

    protected void putGlobally(@NotNull String key, @NotNull Object value) {
        GLOBAL_CONTEXT.put(requireNonNull(key), requireNonNull(value));
    }

    @NotNull
    protected <V> V get(String key) {
        Object res = context.get(key);
        if (res == null) {
            res = GLOBAL_CONTEXT.get(key);
        }
        return requireNonNull((V) res, () ->
                "Cannot find value by key: " + key
        );
    }

    @NotNull
    protected <V> V getAndRemove(String key) {
        Object res = context.remove(key);
        if (res == null) {
            res = GLOBAL_CONTEXT.remove(key);
        }
        return requireNonNull((V) res, () ->
                "Cannot find value by key: " + key
        );
    }

    /**
     * Переход на другую страницу (Page)
     */
    public <P extends BasePage<P>> P goTo(Class<P> page) {
        requireNonNull(page);

        try {
            Constructor<P> constructor = requireNonNull(
                    page.getDeclaredConstructor(Map.class),
                    "Cannot found page constructor"
            );
            return constructor.newInstance(context);
        } catch (ReflectiveOperationException e) {
            throw new IllegalStateException("Cannot instantiate page", e);
        }
    }

    public T attachScreenshot() {
        return attachScreenshot("Screenshot");
    }

    public T attachScreenshot(String name) {
        final byte[] screenshot = getScreenshotBytes();
        if (screenshot == null) {
            throw new RuntimeException("Cannot take screenshot");
        }
        Allure.getLifecycle().addAttachment(name, "image/png", "png", screenshot);

        return (T) this;
    }

    private static byte[] getScreenshotBytes() {
        return WebDriverRunner.hasWebDriverStarted()
                ? ((TakesScreenshot) WebDriverRunner.getWebDriver()).getScreenshotAs(OutputType.BYTES)
                : null;
    }

    private static final Pattern F_SYSTEM_NUMBER_PATTERN = Pattern.compile("^.+_(\\d++)$"); //text_3903

    public static String extractFSystemNumberPattern(String input) {
        Matcher matcher = F_SYSTEM_NUMBER_PATTERN.matcher(input);
        if (!matcher.find()) {
            throw new IllegalArgumentException("Invalid input: " + input);
        }
        return matcher.group(1);
    }

    //******************************************************************************************************************

    /**
     * Ожидание 2 секунды
     */
    protected void waitTime2() {
        sleep(2000L);
    }

    /**
     * Ожидание 5 секунд
     */
    protected void waitTime5() {
        sleep(5000L);
    }

    /**
     * Ожидание 50 секунд
     */
    protected void waitTime50() {
        sleep(50000L);
    }

    /**
     * Перезагружает, обновляет страницу
     */
    public final void refreshPage() {
        refresh();
    }

    /**
     * @param xpath - локатор, по которому ищется элемент и производится клик
     */
    protected void click(final String xpath) {
        $x(xpath).click();
    }

    /**
     * @param xpath - локатор, по которому ищутся элементы и из них извлекается текст
     */
    public List<String> getTextFromElements(final String xpath) {
        ElementsCollection linkLists = $$x(xpath);
        return linkLists.texts();
    }

    public List<String> getTextFromElements(final String xpath, int columnIndex, int columnCount) {
        List<String> allElements = getTextFromElements(xpath);
        List<String> result = new ArrayList<>();
        for (int i = 0; i < allElements.size(); i++) {
            if (i % columnCount == columnIndex) {
                result.add(allElements.get(i));
            }
        }

        return result;
    }

    /**
     * @param xpath - локатор, по которому ищется элемент и производится двойной клик
     */
    protected void doubleClick(final String xpath) {
        $x(xpath).doubleClick();
    }

    /**
     * @param xpath - локатор, по которому ищется элемент и вводится текст
     * @param value - текст
     */
    protected void fillField(final String xpath, final String value) {
        $x(xpath).sendKeys(value);
    }

    /**
     * @param xpath - локатор, по которому ищется элемент и вводится текст
     * @param value - текст
     */
    protected void clearFillField(final String xpath, final String value) {
        $x(xpath).sendKeys(Keys.CONTROL + "a");
        $x(xpath).sendKeys(Keys.DELETE);
        $x(xpath).sendKeys(value);
    }

    /**
     * @param xpath - локатор, по которому ищется элемент
     */
    protected void clearField(final String xpath) {
        $x(xpath).sendKeys(Keys.CONTROL + "a");
        $x(xpath).sendKeys(Keys.DELETE);
    }

    /**
     * @param xpath - локатор, по которому ищется элемент и вводится путь до файла
     * @param value - путь до файла
     */
    protected void uploadFile(final String xpath, final String value) {
        waitTime2();
        System.out.println(">>>> путь до файла: " + value);
        $x(xpath).sendKeys(value);

    }

    /**
     * @param xpath  - локатор, по которому ищется элемент и вводится путь до файла
     * @param values - путь до файла
     */
    protected void uploadFiles(final String xpath, String... values) {
        System.out.println(">>>> путь до файла: " + Arrays.toString(values));
        $x(xpath).sendKeys(values);

    }

    /**
     * клик мышкой по указанным координатам
     */
    protected void clickByCoordinates(final String xcoordinate, final String ycoordinate) {
        ((JavascriptExecutor) webdriver()).executeScript("el = document.elementFromPoint(" + xcoordinate + ", " + ycoordinate + "); el.click();");
    }

    /**
     * Проверка, что элемента НЕТ на странице
     */
    protected void checkNoElement(final String xpath) {
        $x(xpath).shouldBe(Condition.not(Condition.visible));
    }

    /**
     * Проверка, что элемент содержит определенный текст
     */
    protected void checkText(final String xpath, final String text) {
        $x(xpath).shouldBe(Condition.text(text));
    }

    /**
     * Проверка, что элемент НЕ содержит определенный текст
     */
    protected void checkNoText(final String xpath, final String text) {
        $x(xpath).shouldBe(Condition.not(Condition.text(text)));
    }

    /**
     * Проверка, что элемент есть на странице
     */
    protected void checkElementVisible(final String xpath) {
        $x(xpath).shouldBe(Condition.visible);
    }

    /**
     * Проверка, что элемент активен
     */
    protected void checkElementEnabled(final String xpath) {
        $x(xpath).shouldBe(Condition.enabled);
    }

    /**
     * Проверка, что элемент неактивен
     */
    protected void checkElementDisabled(final String xpath) {
        $x(xpath).shouldBe(Condition.disabled);
    }

    /**
     * Найти текст атрибута элемента
     */
    protected String getValueOfAttribute(final String xpath) {
        return $x(xpath).val();
    }

    /**
     * Проверить текст атрибута элемента
     */
    protected void checkTextOfAttribute(final String xpath, final String name, final String text) {
        $x(xpath).shouldHave(attribute(name, text));
    }

    /**
     * Проверить текст атрибута элемента value
     */
    protected void checkTextOfAttributeValue(final String xpath, final String text) {
        $x(xpath).shouldHave(partialValue(text));
    }

    /**
     * Проверить, что атрибут элемента пустой
     */
    protected void checkAttributeNoText(final String xpath) {
        $x(xpath).shouldBe(empty);
    }

    /**
     * Проверка, что элемент содержит любое число
     */
    protected void checkTextIsPositiveDigit(final String xpath) {
        waitTime5();
        String text = $x(xpath).getText();
        assert Pattern.matches("^\\d+,\\d*$", text);
    }

    /**
     * Навести мышку на элемент
     */
    protected void hoverMouse(final String xpath) {
        $x(xpath).hover();
    }
}
