package ru.aliya12.uitestsproject.base;

import io.qameta.allure.Step;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * Created on 08.12.2022
 *
 * @author Aliya Kuznetsova (aliyakuzn@gmail.com)
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class SharedPage<T extends SharedPage> extends BasePage<T> {
    public SharedPage() {
        super();
    }

    public SharedPage(@NotNull Map<String, Object> context) {
        super(context);
    }

    /**
     * Кликнуть по кнопке
     *
     * @param nameButton - имя кнопки
     */
    @Step("Нажать на кнопку '{nameButton}'")
    public T clickButton(final String nameButton) {
        final String xpath = "//button[contains(.,'" + nameButton + "')]";
        checkElementVisible(xpath);
        checkElementEnabled(xpath);
        click(xpath);
        return (T) this;
    }

    /**
     * Кликнуть по кнопке
     *
     * @param classB - class кнопки
     */
    @Step("Нажать на кнопку {classB}")
    public T clickButtonClass(final String classB) {
        final String xpath = "//button[contains(@class,'" + classB + "')]";
        click(xpath);
        return (T) this;
    }

    /**
     * Кликнуть по кнопке
     *
     * @param nameButton - class элемента span
     */
    @Step("Нажать на кнопку '{nameButton}'")
    public T clickButtonDialog(final String nameButton) {
        final String xpath = "//div[contains(@class,'p-dialog-footer')]//button[contains(.,'" + nameButton + "')]";
        click(xpath);
        return (T) this;
    }

    /**
     * Кликнуть по кнопке
     */
    @Step("Нажать на кнопку 'Закрыть всплывающее окно'")
    public T clickButtonClose() {
        final String xpath = "//button[contains(@aria-label,'Close')]";
        click(xpath);
        return (T) this;
    }

    /**
     * Кликнуть по элементу label
     *
     * @param nameL - название элемента
     */
    @Step("Нажать на {nameL}")
    public T clickLabel(final String nameL) {
        final String xpath = "//label[contains(.,'" + nameL + "')]";
        click(xpath);
        return (T) this;
    }

    /**
     * Кликнуть по элементу span
     *
     * @param name имя элемента
     */
    @Step("Нажать на '{name}'")
    public T clickSpan(final String name) {
        final String xpath = "//span[contains(.,'" + name + "')]";
        click(xpath);
        return (T) this;
    }

    /**
     * Кликнуть по элементу td
     *
     * @param name - название элемента
     */
    @Step("Нажать на '{name}'")
    public T clickTd(final String name) {
        final String xpath = "//td[contains(.,'" + name + "')]";
        click(xpath);
        return (T) this;
    }

    /**
     * Нажать чек-бокс
     *
     * @param name имя чек-бокса
     */
    @Step("Нажать: '{name}'")
    public T selectNamedCheckBox(final String name) {
        final String xpath = "//span[contains(.,'" + name + "')]/following-sibling::div[contains(@class,'p-checkbox p-component')]";
        click(xpath);
        return (T) this;
    }

    /**
     * Выбрать элемент списка
     *
     * @param nameLi - название элемента
     */
    @Step("Выбрать: '{nameLi}'")
    public T selectLiItem(final String nameLi) {
        final String xpath = "//li[contains(.,'" + nameLi + "')]";
        checkElementVisible(xpath);
        checkElementEnabled(xpath);
        click(xpath);
        return (T) this;
    }

    /**
     * Выбрать элемент списка
     *
     * @param nameLi - название элемента
     */
    @Step("Выбрать: '{nameLi}'")
    public T selectLiItemDialog(final String nameLi) {
        final String xpath = "//div[contains(@class,'p-dropdown-panel')]//li[contains(.,'" + nameLi + "')]";
        click(xpath);
        return (T) this;
    }

    /**
     * Раскрыть выпадающий список
     *
     * @param className класс элемента
     */
    @Step("Раскрыть выпадающий список")
    public T openDropdownListDiv(final String className) {
        final String xpath = "//div[contains(@class,'" + className + "')]";
        checkElementVisible(xpath);
        checkElementVisible(xpath);
        click(xpath);
        return (T) this;
    }

    /**
     * Раскрыть выпадающий список с названием
     *
     * @param name - имя списка
     */
    @Step("Раскрыть выпадающий список {name}")
    public T openDropdownListName(final String name) {
        final String xpath = "//span[contains(.,'" + name + "')]/following-sibling::div//div[contains(@class,'p-dropdown-trigger')]";
        click(xpath);
        return (T) this;
    }

    /**
     * Нажать на выпадающий список с названием
     *
     * @param name - имя списка
     */
    @Step("Нажать на выпадающий список {name}")
    public T openDropdownListNameDiv(final String name) {
        final String xpath = "//div[contains(.,'" + name + "')]/following-sibling::div//div[contains(@class,'p-dropdown-trigger')]";
        click(xpath);
        return (T) this;
    }

    /**
     * Заполнить поле значением
     *
     * @param className - class поля ввода
     * @param value     - значение
     */
    @Step("Заполнить поле значением '{value}'")
    public T fillFieldInputClass(final String className, final String value) {
        final String xpath = "//input[contains(@class,'" + className + "')]";
        checkElementVisible(xpath);
        checkElementEnabled(xpath);
        click(xpath);
        clearField(xpath);
        fillField(xpath, value);
        return (T) this;
    }

    /**
     * Заполнить поле значением
     *
     * @param role  - role поля ввода
     * @param value - значение
     */
    @Step("Заполнить поле значением '{value}'")
    public T fillFieldInputRole(final String role, final String value) {
        final String xpath = "//input[contains(@role,'" + role + "')]";
        fillField(xpath, value);
        return (T) this;
    }

    /**
     * Заполнить поле значением
     *
     * @param placeholder - placeholder поля ввода
     * @param value       - значение
     */
    @Step("Заполнить поле {placeholder} значением '{value}'")
    public T fillFieldInputP(final String placeholder, final String value) {
        final String xpath = "//input[contains(@placeholder,'" + placeholder + "')]";
        fillField(xpath, value);
        return (T) this;
    }

    /**
     * Заполнить поле значением
     *
     * @param name        - название поля ввода
     * @param placeholder - placeholder поля ввода
     * @param value       - значение
     */
    @Step("Заполнить поле {placeholder} значением '{value}'")
    public T fillFieldInputNameP(final String name, final String placeholder, final String value) {
        final String xpath = "//span[contains(.,'" + name + "')]/following-sibling::span//input[contains(@placeholder,'" + placeholder + "')]";
        fillField(xpath, value);
        return (T) this;
    }

    /**
     * Заполнить поле значением
     *
     * @param inputMode - inputmode поля ввода
     * @param value     - значение
     */
    @Step("Заполнить поле значением '{value}'")
    public T fillFieldInputMode(final String inputMode, final String value) {
        final String xpath = "//input[contains(@inputmode,'" + inputMode + "')]";
        fillField(xpath, value);
        return (T) this;
    }

    /**
     * Заполнить поле значением
     *
     * @param id    - id поля ввода
     * @param value - значение
     */
    @Step("Заполнить поле значением '{value}'")
    public T fillFieldInputId(final String id, final String value) {
        final String xpath = "//input[contains(@id,'" + id + "')]";
        checkElementVisible(xpath);
        checkElementEnabled(xpath);
        click(xpath);
        clearField(xpath);
        fillField(xpath, value);
        return (T) this;
    }

    /**
     * Заполнить поле значением
     *
     * @param name  - имя поля ввода
     * @param value - значение
     */
    @Step("Заполнить поле '{name}' значением '{value}'")
    public T fillFieldInputSpan(final String name, final String value) {
        final String xpath = "//span[contains(.,'" + name + "')]/following-sibling::input[contains(@class,'inputtext')]";
        clearFillField(xpath, value);
        return (T) this;
    }

    /**
     * Заполнить поле значением
     *
     * @param name  - имя поля ввода
     * @param value - значение
     */
    @Step("Заполнить поле '{name}' значением '{value}'")
    public T fillFieldInputLabel(final String name, final String value) {
        final String xpath = "//label[contains(.,'" + name + "')]/following-sibling::input[contains(@class,'inputtext')]";
        clearFillField(xpath, value);
        return (T) this;
    }

    /**
     * Заполнить поле значением
     *
     * @param fieldId Id элемента
     * @param value   - значение
     */
    @Step("Заполнить поле значением '{value}")
    public T fillFieldTextareaId(final String fieldId, final String value) {
        final String xpath = "//textarea[contains(@id,'" + fieldId + "')]";
        clearFillField(xpath, value);
        return (T) this;
    }

    /**
     * Очистить поле
     *
     * @param fieldId Id элемента
     */
    @Step("Очистить поле")
    public T clearFieldTextareaId(final String fieldId) {
        final String xpath = "//textarea[contains(@id,'" + fieldId + "')]";
        clearField(xpath);
        return (T) this;
    }

    /**
     * Очистить поле
     *
     * @param placeholder - placeholder поля ввода
     */
    @Step("Очистить поле {placeholder}")
    public T clearFieldInputP(final String placeholder) {
        final String xpath = "//input[contains(@placeholder,'" + placeholder + "')]";
        clearField(xpath);
        return (T) this;
    }

    /**
     * Проверить значение поля
     *
     * @param classEl - class элемента
     * @param text    - проверяемый текст
     */
    @Step("Проверка, что поле содержит значение '{text}'")
    public T checkTextSpanClass(final String classEl, final String text) {
        final String xpath = "//span[contains(@class,'" + classEl + "')]";
        checkText(xpath, text);
        return (T) this;
    }

    /**
     * Проверить значение поля
     *
     * @param classEl - class элемента
     * @param text    - проверяемый текст
     */
    @Step("Проверка, что поле содержит значение '{text}'")
    public T checkTextDivClass(final String classEl, final String text) {
        final String xpath = "//div[contains(@class,'" + classEl + "')]";
        checkText(xpath, text);
        return (T) this;
    }

    /**
     * Проверить значение поля
     *
     * @param id   - id элемента
     * @param text - проверяемый текст
     */
    @Step("Проверка, что поле содержит значение '{text}'")
    public T checkTextDivId(final String id, final String text) {
        final String xpath = "//div[contains(@id,'" + id + "')]";
        checkText(xpath, text);
        return (T) this;
    }

    /**
     * Проверить значение поля
     *
     * @param classEl - class элемента
     * @param value   - проверяемый текст
     */
    @Step("Проверка, что поле содержит значение '{value}'")
    public T checkTextDiv(final String classEl, final String value) {
        final String xpath = "//div[contains(@class,'" + classEl + "')][contains(.,'" + value + "')]";
        checkElementVisible(xpath);
        return (T) this;
    }

    /**
     * Проверить значение поля
     *
     * @param text - проверяемый текст
     */
    @Step("Проверка, что поле содержит значение '{text}'")
    public T checkTextTd(final String text) {
        final String xpath = "//td[contains(.,'" + text + "')]";
        checkElementVisible(xpath);
        checkText(xpath, text);
        return (T) this;
    }


    /**
     * Проверить значение поля
     *
     * @param id   - id элемента
     * @param text - проверяемый текст
     */
    @Step("Проверка, что поле содержит значение '{text}'")
    public T checkTextDivStrong(final String id, final String text) {
        final String xpath = "//div[contains(@id,'" + id + "')]/strong";
        checkText(xpath, text);
        return (T) this;
    }

    /**
     * Проверить значение поля
     *
     * @param forEl - for элемента
     * @param text  - проверяемый текст
     */
    @Step("Проверка, что поле содержит значение '{text}'")
    public T checkTextLabelFor(final String forEl, final String text) {
        final String xpath = "//label[contains(@for,'" + forEl + "')]";
        checkText(xpath, text);
        return (T) this;
    }

    /**
     * Проверить значение поля
     *
     * @param className - class элемента
     * @param text      - проверяемый текст
     */
    @Step("Проверка, что поле содержит значение '{text}'")
    public T checkTextInputValue(final String className, final String text) {
        final String xpath = "//input[contains(@class,'" + className + "')]";
        checkTextOfAttributeValue(xpath, text);
        return (T) this;
    }

    /**
     * Проверить значение поля
     *
     * @param className - class элемента
     */
    @Step("Проверка, что поле пустое")
    public T checkNoTextInputValue(final String className) {
        final String xpath = "//input[contains(@class,'" + className + "')]";
        checkAttributeNoText(xpath);
        return (T) this;
    }

    /**
     * Проверить значение поля
     *
     * @param name - имя элемента
     * @param text - проверяемый текст
     */
    @Step("Проверка, что поле '{name}' содержит значение '{text}'")
    public T checkTextSpanB(final String name, final String text) {
        final String xpath = "//span[contains(., '" + name + "')]//b";
        checkText(xpath, text);
        return (T) this;
    }

    /**
     * Проверить значение поля
     *
     * @param forEl - for элемента
     * @param text  - проверяемый текст
     */
    @Step("Проверка, что поле содержит значение '{text}'")
    public T checkTextLabelDiv(final String forEl, final String text) {
        final String xpath = "//label[contains(@for,'" + forEl + "')]/following-sibling::div";
        checkText(xpath, text);
        return (T) this;
    }

    /**
     * Проверить значение поля
     *
     * @param name - название элемента
     * @param text - проверяемый текст
     */
    @Step("Проверка, что поле содержит значение '{text}'")
    public T checkTextValueLabelInput(final String name, final String text) {
        final String xpath = "//label[contains(.,'" + name + "')]/following-sibling::input[contains(@class,'inputtext')]";
        checkTextOfAttributeValue(xpath, text);
        return (T) this;
    }

    /**
     * Проверить, что элемент есть на странице
     *
     * @param name текст
     */
    @Step("Проверить, что элемент: '{name}' присутствует на странице")
    public T checkSpan(final String name) {
        final String xpath = "//span[contains(.,'" + name + "')]";
        checkElementVisible(xpath);
        return (T) this;
    }

    /**
     * Проверить, что элемента НЕТ на странице
     *
     * @param classEl - class элемента
     * @param value   - проверяемый текст
     */
    @Step("Проверка, что поле НЕ содержит значение '{value}'")
    public T checkNoTextDiv(final String classEl, final String value) {
        final String xpath = "//div[contains(@class,'" + classEl + "')][contains(.,'" + value + "')]";
        checkNoElement(xpath);
        return (T) this;
    }

    /**
     * Проверить, что элемента НЕТ на странице
     *
     * @param text - проверяемый текст
     */
    @Step("Проверка, что поле НЕ содержит значение '{text}'")
    public T checkNoTextTD(final String text) {
        final String xpath = "//td[contains(.,'" + text + "')]";
        checkNoElement(xpath);
        return (T) this;
    }

    /**
     * Проверить, что поле ввода неактивно
     *
     * @param fieldId Id элемента
     */
    @Step("Проверить, что поле ввода неактивно")
    public T checkFieldDisabled(final String fieldId) {
        final String xpath = "//textarea[contains(@id,'" + fieldId + "')]";
        checkElementDisabled(xpath);
        return (T) this;
    }

    /**
     * Проверить, что поле ввода активно
     *
     * @param fieldId Id элемента
     */
    @Step("Проверить, что поле ввода активно")
    public T checkFieldEnabled(final String fieldId) {
        final String xpath = "//textarea[contains(@id,'" + fieldId + "')]";
        checkElementEnabled(xpath);
        return (T) this;
    }

    /**
     * Проверить, что кнопка НЕ активна
     *
     * @param nameButton имя элемента
     */
    @Step("Проверить, что кнопка '{nameButton}' не активна")
    public T checkButtonDisabled(final String nameButton) {
        final String xpath = "//button[contains(.,'" + nameButton + "')]";
        checkElementDisabled(xpath);
        return (T) this;
    }

    /**
     * Проверить, что кнопка активна
     *
     * @param nameButton имя элемента
     */
    @Step("Проверить, что кнопки '{nameButton}' нет на странице")
    public T checkButtonEnabled(final String nameButton) {
        final String xpath = "//button[contains(.,'" + nameButton + "')]";
        checkElementEnabled(xpath);
        return (T) this;
    }

    /**
     * Проверить, что кнопка активна
     *
     * @param nameButton имя элемента
     */
    @Step("Проверить, что кнопка '{nameButton}' активна")
    public T checkButtonDialogEnabled(final String nameButton) {
        final String xpath = "//div[contains(@class,'p-dialog-footer')]//button[contains(.,'" + nameButton + "')]";
        checkElementEnabled(xpath);
        return (T) this;
    }

    /**
     * Проверить, что кнопка НЕ активна
     *
     * @param nameButton имя элемента
     */
    @Step("Проверить, что кнопка '{nameButton}' не активна")
    public T checkButtonDialogDisabled(final String nameButton) {
        final String xpath = "//div[contains(@class,'p-dialog-footer')]//button[contains(.,'" + nameButton + "')]";
        checkElementDisabled(xpath);
        return (T) this;
    }

    /**
     * Навести мышку на кнопку
     *
     * @param nameButton имя элемента
     */
    @Step("Проверить, что кнопка '{nameButton}' не активна")
    public T hoverMouseOverButton(final String nameButton) {
        final String xpath = "//button[contains(.,'" + nameButton + "')]";
        hoverMouse(xpath);
        return (T) this;
    }

}
