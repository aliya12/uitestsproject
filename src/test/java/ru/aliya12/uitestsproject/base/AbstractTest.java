package ru.aliya12.uitestsproject.base;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import static com.codeborne.selenide.Selenide.open;

/**
 * Created on 08.12.2022
 *
 * @author Aliya Kuznetsova (aliyakuzn@gmail.com)
 */
public abstract class AbstractTest {
    @BeforeAll
    public static void setUp() {
        //closeWebDriver();
        Configuration.baseUrl = "http://.../";
        Configuration.browser = "firefox";
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(true));
    }

    protected <P extends ru.aliya12.uitestsproject.base.BasePage<P>> P startWith(String relativeOrAbsoluteUrl, Class<P> page) {
        return open(relativeOrAbsoluteUrl, page);
    }

    @AfterAll
    public static void cleanupTest() {
        WebDriverRunner.closeWebDriver();
    }
}
