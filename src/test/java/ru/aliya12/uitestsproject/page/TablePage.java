package ru.aliya12.uitestsproject.page;

import io.qameta.allure.Step;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import ru.aliya12.uitestsproject.base.NaturalOrderComparator;
import ru.aliya12.uitestsproject.base.SharedPage;
import ru.aliya12.uitestsproject.base.SortOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created on 11.01.2023
 *
 * @author Aliya Kuznetsova (aliyakuzn@gmail.com)
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class TablePage extends SharedPage<TablePage> {
    public TablePage() {
        super();
    }

    public TablePage(@NotNull Map context) {
        super(context);
    }

    /**
     * Раскрыть выпадающий список
     */
    @Step("Раскрыть выпадающий список")
    public TablePage openDropdownListPageNumber() {
        final String xpath = "//div[contains(@aria-haspopup,'listbox')]";
        click(xpath);
        return this;
    }

    /**
     * Проверить значение поля
     *
     * @param text  - проверяемый текст
     * @param index - индекс элемента
     */
    @Step("Проверка, что поле содержит значение '{text}'")
    public TablePage checkThValue(final String text, final String index) {
        final String xpath = "//th[contains(@class,'classname')][" + index + "]";
        checkTextOfAttribute(xpath, "aria-sort", text);
        return this;
    }

    /**
     * Запомнить порядок элементов в таблице
     */
    @Step("Запомнить порядок элементов в таблице")
    public TablePage rememberElements(String xpath) {
        put(xpath, getTextFromElements(xpath));
        return this;
    }

    /**
     * Запомнить порядок элементов в таблице
     */
    @Step("Запомнить порядок элементов в таблице")
    public TablePage rememberElements(String xpath, int columnIndex, int columnCount) {
        put(xpath, getTextFromElements(xpath, columnIndex, columnCount));
        return this;
    }

    /**
     * Проверить порядок элементов в таблице
     */
    @Step("Проверка, что элементы в таблице отсортированы по {sortOrder}")
    public TablePage checkSortOrderOfElements(String xpath, int columnIndex, int columnCount, SortOrder sortOrder) {
        //final String xpath = "//td[contains(@role,'cell')]";

        List<String> elements = getTextFromElements(xpath, columnIndex, columnCount);
        switch (sortOrder) {
            case ASCENDING: {
                List<String> sortedElements = new ArrayList<>(elements);
                sortedElements.sort(String::compareToIgnoreCase);
                Assertions.assertEquals(sortedElements, elements);
                break;
            }
            case DESCENDING: {
                List<String> sortedElements = new ArrayList<>(elements);
                sortedElements.sort((o1, o2) -> -o1.compareToIgnoreCase(o2));
                Assertions.assertEquals(sortedElements, elements);
                break;
            }
            case UNSORTED: {
                List<String> originElements = getAndRemove(xpath);
                Assertions.assertEquals(originElements, elements);
                break;
            }
        }

        return this;
    }

    /**
     * Проверить порядок элементов в таблице
     */
    @Step("Проверка, что элементы в таблице отсортированы по {sortOrder}")
    public TablePage checkSortOrderOfElementsInt(String xpath, int columnIndex, int columnCount, SortOrder sortOrder) {
        //final String xpath = "//td[contains(@role,'cell')]";

        List<String> elements = getTextFromElements(xpath, columnIndex, columnCount);
        switch (sortOrder) {
            case ASCENDING: {
                List<String> sortedElements = new ArrayList<>(elements);
                sortedElements.sort(new NaturalOrderComparator());
                Assertions.assertEquals(sortedElements, elements);
                break;
            }
            case DESCENDING: {
                List<String> sortedElements = new ArrayList<>(elements);
                sortedElements.sort(new NaturalOrderComparator().reversed());
                Assertions.assertEquals(sortedElements, elements);
                break;
            }
            case UNSORTED: {
                List<String> originElements = getAndRemove(xpath);
                Assertions.assertEquals(originElements, elements);
                break;
            }
        }

        return this;
    }
}

