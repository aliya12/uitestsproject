package ru.aliya12.uitestsproject.page;

import io.qameta.allure.Step;
import org.jetbrains.annotations.NotNull;
import ru.aliya12.uitestsproject.base.SharedPage;

import java.util.Map;

/**
 * Created on 20.12.2022
 *
 * @author Aliya Kuznetsova (aliyakuzn@gmail.com)
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class DatePickerPage extends SharedPage<DatePickerPage> {
    public DatePickerPage() {
        super();
    }

    public DatePickerPage(@NotNull Map context) {
        super(context);
    }

    /**
     * Раскрыть выпадающий список календаря
     *
     * @param className класс элемента
     */
    @Step("Раскрыть выпадающий список календаря")
    public DatePickerPage openDropdownListDate(final String className) {
        final String xpath = "//div[contains(@class,'p-datepicker-title')]//div[contains(@class,'" + className + "')]";
        click(xpath);
        return this;
    }

    /**
     * Раскрыть один из нескольких выпадающих списков календаря
     *
     * @param className класс элемента
     */
    @Step("Раскрыть выпадающий список календаря")
    public DatePickerPage openSomeDropdownListDate(final String className) {
        final String xpath = "//div[contains(@class,'p-datepicker-title')]/div[contains(@class,'" + className + "')]//div[contains(@class,'p-dropdown-trigger')]";
        click(xpath);
        return this;
    }

    /**
     * Раскрыть календарь
     */
    @Step("Раскрыть календарь")
    public DatePickerPage openCalendar() {
        final String xpath = "//span[contains(@class,'calendar ')]";
        click(xpath);
        return this;
    }

    /**
     * Выбрать число в календаре
     *
     * @param name имя элемента
     */
    @Step("Выбрать число '{name}'")
    public DatePickerPage clickDateSpan(final String name) {
        final String xpath = "//table[contains(@class,'p-datepicker-calendar')]//span[contains(.,'" + name + "')]";
        click(xpath);
        return this;
    }

}
