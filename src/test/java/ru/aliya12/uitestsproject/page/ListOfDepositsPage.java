package ru.aliya12.uitestsproject.page;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import org.jetbrains.annotations.NotNull;
import ru.aliya12.uitestsproject.base.BasePage;
import ru.aliya12.uitestsproject.base.Key;
import ru.aliya12.uitestsproject.base.SharedPage;

import java.util.Map;

import static com.codeborne.selenide.Selenide.$x;

/**
 * Created on 08.12.2022
 *
 * @author Aliya Kuznetsova (aliyakuzn@gmail.com)
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class ListOfDepositsPage extends SharedPage<ListOfDepositsPage> {
    public ListOfDepositsPage() {
        super();
    }

    public ListOfDepositsPage(@NotNull Map context) {
        super(context);
    }

    /**
     * Кликнуть по элементу списка
     *
     * @param name имя элемента
     */
    @Step("Нажать на '{name}'")
    public ListOfDepositsPage clickSpanButton(final String name) {
        final String xpath = "//span[contains(.,'" + name + "')]/preceding-sibling::button";
        click(xpath);
        return this;
    }

    /**
     * Нажать на кнопку
     *
     * @param name имя элемента
     */
    @Step("Нажать на кнопку: '{name}'")
    public ListOfDepositsPage clickUpSpanButton(final String name) {
        final String xpath = "//span[contains(.,'" + name + "')]/ancestor::button";
        click(xpath);
        return this;
    }

    /**
     * Выбрать шаблон по ...
     *
     * @return - возвращает ту же страницу, на которой были
     */
    @Step("Выбрать шаблон по Id")
    public ListOfDepositsPage chooseFSystemNumber() {
        String number = get(Key.F_SYSTEM_NUMBER);
        final String xpath = "//span[contains(.,'" + number + "')]/preceding-sibling::div[contains(@class,'classname')]";
        click(xpath);
        return this;
    }

    /**
     * Извлечь номер из названия ....
     *
     * @param placeholder - placeholder поля ввода
     */
    @Step("Извлечь номер из названия ....")
    public ListOfDepositsPage extractFSystemNumber(final String placeholder) {
        final String xpath = "//input[contains(@placeholder, '" + placeholder + "')]";
        final String text = getValueOfAttribute(xpath);
        putGlobally(Key.F_SYSTEM_NUMBER, BasePage.extractFSystemNumberPattern(text));
        System.out.println(">>>>>>>>>>>>>>>> F_SYSTEM_NUMBER = " + get(Key.F_SYSTEM_NUMBER));
        return this;
    }

    /**
     * Очистить выбранные ...
     *
     * @param className класс элемента
     */
    @Step("Очистить выбранные ...")
    public ListOfDepositsPage clearSystems(final String className) {
        final String xpath = "//i[contains(@class,'" + className + "')]";
        if ($x(xpath).is(Condition.visible)) {
            click(xpath);
        }
        return this;
    }

    /**
     * Проверить значение поля с номером ....
     *
     * @param id - id элемента
     */
    @Step("Проверить, что ....")
    public ListOfDepositsPage checkTextDivIdNumber(final String id) {
        final String xpath = "//div[contains(@id,'" + id + "')]";
        final String text = get(Key.F_SYSTEM_NUMBER);
        checkText(xpath, text);
        return this;
    }

    /**
     * Выбрать все ...
     *
     * @param className имя элемента
     */
    @Step("Выбрать все ....")
    public ListOfDepositsPage clickElsDiv(final String className) {
        final String xpath = "//div[contains(@class,'" + className + "')]";
        click(xpath);
        return this;
    }

    /**
     * Раскрыть выпадающий список
     *
     * @param className класс элемента
     */
    @Step("Раскрыть выпадающий список")
    public ListOfDepositsPage openDropdownList(final String className) {
        final String xpath = "//span[contains(@class,'" + className + "')]";
        click(xpath);
        return this;
    }

    /**
     * Проверить значение поля с номером ...
     *
     * @param className класс элемента
     */
    @Step(".....")
    public ListOfDepositsPage checkLiTextNumber(final String className) {
        final String xpath = "//li[contains(@class,'" + className + "')]";
        final String text = get(Key.F_SYSTEM_NUMBER);
        checkText(xpath, text);
        return this;
    }

    /**
     * Проверить, что кнопка есть на странице
     *
     * @param nameButton имя элемента
     */
    @Step("Проверить, что кнопки '{nameButton}' нет на странице")
    public ListOfDepositsPage checkButtonVisible(final String nameButton) {
        final String xpath = "//button[contains(.,'" + nameButton + "')]";
        checkElementVisible(xpath);
        return this;
    }

    /**
     * Проверить, есть ли ... и если нет, то создать
     *
     * @return - возвращает ту же страницу, на которой были
     */
    @Step("Проверить, есть ли на странице ... и если нет, создать")
    public ListOfDepositsPage chooseTacticAverageRepair() {
        final String xpath1 = "//li[contains(.,'text')]";
        if ($x(xpath1).is(Condition.visible)) {
            click(xpath1);
        } else {
            selectLiItem("name")
                    .openDropdownListNameDiv("name")
                    .selectLiItem("name")
                    .openDropdownListNameDiv("name")
                    .selectLiItem("name")
                    .clickButtonDialog("name");
        }
        return this;
    }
}
