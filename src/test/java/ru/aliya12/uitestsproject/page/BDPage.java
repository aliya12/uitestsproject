package ru.aliya12.uitestsproject.page;

import io.qameta.allure.Step;
import org.jetbrains.annotations.NotNull;
import ru.aliya12.uitestsproject.base.BasePage;
import ru.aliya12.uitestsproject.base.Key;

import java.sql.*;
import java.util.Map;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created on 21.12.2022
 *
 * @author Aliya Kuznetsova (aliyakuzn@gmail.com)
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class BDPage extends BasePage<BDPage> {

    public BDPage() {
        super();
    }

    public BDPage(@NotNull Map context) {
        super(context);
    }

    /**
     * Найти ... в БД
     *
     * @return - возвращает ту же страницу, на которой были
     */
    @Step("Найти ... в таблице '...'")
    public BDPage findMaintenanceStrategyId(final String description) {

        String connectionUrl =
                "jdbc:postgresql://127.0.0.1:5432/vertex";
        Properties props = new Properties();
        props.setProperty("user", "username");
        props.setProperty("password", "password");
        props.setProperty("options", "-c statement_timeout=30000");

        try (Connection connection = DriverManager.getConnection(connectionUrl, props);
             Statement statement = connection.createStatement()) {
            String selectSql =
                    "SELECT ... FROM ...  WHERE ... = '" + description + "'";
            ResultSet resultSet = statement.executeQuery(selectSql);
            assertTrue(resultSet.next());

            final String maintenanceStrategyId = resultSet.getString(1);
            put(Key.MAINTENANCE_STRATEGY_ID, maintenanceStrategyId);
            System.out.println(">>>>>>>>>>>>>>>> MAINTENANCE_STRATEGY_ID = " + get(Key.MAINTENANCE_STRATEGY_ID));

            assertFalse(resultSet.next());
        } catch (SQLException e) {
            throw new RuntimeException("SQL failed", e);
        }
        return this;
    }

    /**
     * Удалить ...
     *
     * @return - возвращает ту же страницу, на которой были
     */
    @Step("Удалить Стратегию обслуживания")
    public BDPage deleteMaintenanceStrategy() {
        final String maintenanceStrategyId = get(Key.MAINTENANCE_STRATEGY_ID);
        String connectionUrl =
                "jdbc:postgresql://127.0.0.1:5432/vertex";
        Properties props = new Properties();
        props.setProperty("user", "username");
        props.setProperty("password", "password");
        props.setProperty("options", "-c statement_timeout=30000");

        try (Connection connection = DriverManager.getConnection(connectionUrl, props);
             Statement statement = connection.createStatement()) {
            String deleteSql1 =
                    "DELETE FROM ... " +
                            "WHERE ...='" + maintenanceStrategyId + "'";
            int res1 = statement.executeUpdate(deleteSql1);
            assertEquals(1, res1);

            String deleteSql2 =
                    "DELETE FROM ... " +
                            "WHERE ...='" + maintenanceStrategyId + "'";
            int res2 = statement.executeUpdate(deleteSql2);
            assertEquals(1, res2);

        } catch (SQLException e) {
            throw new RuntimeException("SQL failed", e);
        }
        return this;
    }

}
