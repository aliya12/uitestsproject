package ru.aliya12.uitestsproject.page;

import io.qameta.allure.Step;
import org.jetbrains.annotations.NotNull;
import ru.aliya12.uitestsproject.base.SharedPage;

import java.util.Map;

/**
 * Created on 08.12.2022
 *
 * @author Aliya Kuznetsova (aliyakuzn@gmail.com)
 */
public class LoginPage extends SharedPage<LoginPage> {
    public LoginPage() {
        super();
    }

    public LoginPage(@NotNull Map<String, Object> context) {
        super(context);
    }

    /**
     * Заполнить поле значением
     *
     * @param value - значение
     */
    @Step("Заполнить поле значением '{value}'")
    public LoginPage fillFieldInput(final String type, final String value) {
        String xpath = "//input[contains(@type, '" + type + "')]";
        fillField(xpath, value);
        return this;
    }

}
