package ru.aliya12.uitestsproject.test;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.aliya12.uitestsproject.base.AbstractTest;
import ru.aliya12.uitestsproject.base.SortOrder;
import ru.aliya12.uitestsproject.page.ListOfDepositsPage;
import ru.aliya12.uitestsproject.page.LoginPage;
import ru.aliya12.uitestsproject.page.TablePage;

/**
 * Created on 11.01.2023
 *
 * @author Aliya Kuznetsova (aliyakuzn@gmail.com)
 */
public class Test2 extends AbstractTest {
    @Test
    @Epic("Epic")
    @Feature("Feature")
    @DisplayName("DisplayName")
    @Description("Description")
    public void test2() {
        startWith("/", LoginPage.class)
                .fillFieldInput("text", "")
                .fillFieldInput("password", "")
                .clickButton("name")
                .goTo(ListOfDepositsPage.class)
                .clickSpan("name")
                .clickSpanButton("name")
                .clickSpanButton("name")
                .clickSpan("name")
                .goTo(TablePage.class)
                .openDropdownListPageNumber()
                .selectLiItem("number")
                .checkThValue("none", "1")
                .rememberElements("//td[contains(@role,'cell')]", 1, 9)
                .clickSpan("name")
                .checkThValue("ascending", "1")
                .checkSortOrderOfElements("//td[contains(@role,'cell')]", 1, 9, SortOrder.ASCENDING)
                .clickSpan("name")
                .checkThValue("descending", "1")
                .checkSortOrderOfElements("//td[contains(@role,'cell')]", 1, 9, SortOrder.DESCENDING)
                .clickSpan("name")
                .checkThValue("none", "1")
                .checkSortOrderOfElements("//td[contains(@role,'cell')]", 1, 9, SortOrder.UNSORTED)
                .checkThValue("none", "2")
                .checkThValue("none", "5")
                .rememberElements("//td[contains(@role,'cell')]", 5, 9)
                .clickSpan("name1")
                .checkThValue("ascending", "5")
                .checkSortOrderOfElements("//td[contains(@role,'cell')]", 5, 9, SortOrder.ASCENDING)
                .clickSpan("name1")
                .checkThValue("descending", "5")
                .checkSortOrderOfElements("//td[contains(@role,'cell')]", 5, 9, SortOrder.DESCENDING)
                .clickSpan("name1")
                .checkThValue("none", "5")
                .checkSortOrderOfElements("//td[contains(@role,'cell')]", 5, 9, SortOrder.UNSORTED)
        ;
    }
}
