package ru.aliya12.uitestsproject.test;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.aliya12.uitestsproject.base.AbstractTest;
import ru.aliya12.uitestsproject.page.ListOfDepositsPage;
import ru.aliya12.uitestsproject.page.LoginPage;

/**
 * Created on 13.12.2022
 *
 * @author Aliya Kuznetsova (aliyakuzn@gmail.com)
 */
public class Test1 extends AbstractTest {
    int a = 1000;
    int b = 9999;
    int random_number = a + (int) (Math.random() * b);

    @Test
    @Epic("Epic")
    @Feature("Feature")
    @DisplayName("DisplayName")
    @Description("Description")
    public void test1() {
        startWith("/", LoginPage.class)
                .fillFieldInput("text", "")
                .fillFieldInput("password", "")
                .clickButton("Войти")
                .goTo(ListOfDepositsPage.class)
                .clickSpan("Учет")
                .clickSpanButton("name")
                .clickSpanButton("name")
                .clickSpanButton("name")
                .clickSpan("name")
                .checkButtonVisible("name")
                .checkButtonVisible("name")
                .checkButtonVisible("name")
                .checkButtonVisible("name")
                .checkButtonVisible("name")
                .checkButtonVisible("name")
                .checkButtonVisible("name")
                .checkTextTd("name")
                .checkTextTd("name")
                .checkTextTd("name")
                .checkTextTd("name")
                .clickButton("name")
                .clearSystems("classname")
                .clickUpSpanButton("name")
                .fillFieldInputP("name", "testauto_" + random_number + "")
                .extractFSystemNumber("name")
                .clickButtonClass("classname")
                .clickUpSpanButton("name")
                .fillFieldInputP("name", "testauto_" + random_number + "")
                .extractFSystemNumber("name")
                .clickButtonClass("classname")
                .openDropdownList("classname")
                .chooseFSystemNumber()
                .openDropdownList("classname")
                .clickButton("name")
                .checkTextDivIdNumber("id")
                .clickElsDiv("classname")
                .clickButton("name")
                .openDropdownListDiv("classname")
                .checkLiTextNumber("classname")
        ;
    }
}
